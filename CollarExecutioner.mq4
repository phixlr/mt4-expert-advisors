//+------------------------------------------------------------------+
//|                                            CollarExecutioner.mq4 |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   string TimeWithSeconds=TimeToStr(TimeLocal(), TIME_DATE|TIME_SECONDS);
   
   if(TimeWithSeconds == "2019.05.10 04:29:58") {
      Alert("I should execyte 40");
   }
   Comment("Time: ",TimeWithSeconds);  
  }
//+------------------------------------------------------------------+
