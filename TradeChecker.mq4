//+------------------------------------------------------------------+
//|                                                 TradeChecker.mq4 |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

double pipNumberProtector = (5*10)*Point; // First value inside param is the pips
double minimumPipForProtection = (1*10)*Point; // First value inside param is the pips
bool isProtectionActivated = false;

bool CheckOpenOrders(){
   for( int i = 0 ; i < OrdersTotal() ; i++ ) {
      if(OrderSelect( i, SELECT_BY_POS, MODE_TRADES )) {
         if( OrderSymbol() == Symbol() && OrderType() <= 1 ) return(true);
      }
   }

   return(false);
}

void DeletePendingOrders() {
 int total = OrdersTotal();
  for(int i=total-1;i>=0;i--)
  {
    if(OrderSelect(i, SELECT_BY_POS)) {
       if(OrderSymbol() == Symbol() && OrderType() >= 2) {
         bool isOrderDeleted = OrderDelete(OrderTicket());
       }
    }
  }
}

void AutoProtection() {
   /*************************************
   * This method will adjust the stop loss
   * as soon as the price went 5 pips (50 points)
   * away from the entry price.
   * The stoploss will be set 1 pip away from entry price
   *************************************/
   for( int i = 0 ; i < OrdersTotal() ; i++ ) {
      if(OrderSelect( i, SELECT_BY_POS, MODE_TRADES )) {
         if( OrderSymbol() == Symbol() && OrderType() <= 1 ) {
            double marketPrice = NormalizeDouble(((Ask+Bid)/2),(Digits-1));
            if(isProtectionActivated == false && OrderType() == OP_BUY && marketPrice > (marketPrice+pipNumberProtector)) {
               if(OrderModify(OrderTicket(),OrderOpenPrice(),NormalizeDouble(OrderOpenPrice()+minimumPipForProtection,Digits),OrderTakeProfit(),0,Blue)) isProtectionActivated = true;
            } else if (isProtectionActivated == false && OrderType() == OP_SELL && marketPrice < (marketPrice-pipNumberProtector)) {
               if(OrderModify(OrderTicket(),OrderOpenPrice(),NormalizeDouble(OrderOpenPrice()-minimumPipForProtection,Digits),OrderTakeProfit(),0,Blue)) isProtectionActivated = true;
            }
         }
      }
   }
}

//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   if(CheckOpenOrders() == true) {
      DeletePendingOrders();
      /**if(isProtectionActivated == false) {
bool fm=OrderModify(OrderTicket(),OrderOpenPrice(),5,OrderTakeProfit(),0,Red);
if(fm) {
isProtectionActivated = true;
}**/
         /**for( int i = 0 ; i < OrdersTotal() ; i++ ) {
            if(OrderSelect( i, SELECT_BY_POS, MODE_TRADES )) {
               if(OrderModify(OrderTicket(),OrderOpenPrice(),NormalizeDouble(OrderOpenPrice()-((1*10)*Point),Digits),OrderTakeProfit(),0,Blue)) {
                  isProtectionActivated = true;
               }
            }
         } **/
       //}
      //AutoProtection();
   }
  }
//+------------------------------------------------------------------+
