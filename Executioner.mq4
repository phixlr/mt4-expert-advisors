//+------------------------------------------------------------------+
//|                                                  Executioner.mq4 |
//|                        Copyright 2019, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2019, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

input double lots=0;
input double pips=6;
input double price;

double marketPrice;
double volume;
double risk=0.03;
extern double MaxRiskPerTrade=3;
double amountRisk;
bool isOrderPlaced = false;

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- create timer
   EventSetTimer(1);
   
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//--- destroy timer
   EventKillTimer();
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer()
  {
//---
   string TimeWithSeconds=TimeToStr(TimeLocal(), TIME_DATE|TIME_SECONDS);
   
   Comment("Time: ",TimeWithSeconds);  
   
   if(TimeWithSeconds == "2019.05.10 11:17:58" && isOrderPlaced == false) {
      Alert("Placing Orders!");
      PlaceOrders();
       EventKillTimer();
   }
   
  }
//+------------------------------------------------------------------+

double CalculateLotSize(double SL){          //Calculate the size of the position size 
   double LotSize=0;
   //We get the value of a tick
   double nTickValue=MarketInfo(Symbol(),MODE_TICKVALUE);
   //If the digits are 3 or 5 we normalize multiplying by 10
   if(Digits==3 || Digits==5){
      nTickValue=nTickValue*10;
   }
   //We apply the formula to calculate the position size and assign the value to the variable
   amountRisk = AccountBalance()*MaxRiskPerTrade/100;
   LotSize=(amountRisk)/(SL*nTickValue);
   return LotSize;
}

void PlaceOrders() {
    string quoteCurrency = StringSubstr((string)Symbol(), 3, 3);
   string baseCurrency = StringSubstr((string)Symbol(), 0, 3);

   if(lots == 0) {
      volume = CalculateLotSize(pips);      
   } else {
      volume = lots;
   }


   // If price is left empty, calculate the market price
   if(price==0) {
    marketPrice = NormalizeDouble(((Ask+Bid)/2),(Digits-1));
   } else {
    marketPrice = price;
   }
   
   // Multiply the pips by 10 and multiply by the Point on current pair (JPY)
   double points = (pips*10)*Point;
   double sellstopprice=NormalizeDouble(Bid-points,Digits);
   double buystopprice=NormalizeDouble(Ask+points,Digits);
   
   // Place the sell stop order
   int sellstop=OrderSend(
      Symbol(),
      OP_SELLSTOP,
      volume,
      sellstopprice,
      0,
      Bid,
      0,
      "SS-"+(string)Symbol()+"-NS",
      1,
      0,
      clrRed
   );
      
   // Place the buy stop order
   int buystop=OrderSend(
      Symbol(),
      OP_BUYSTOP,
      volume,
      buystopprice,
      0,
      Ask,
      0,
      "BS-"+(string)Symbol()+"-NS",
      1,
      0,
      clrGreen
   );
   
   if(buystop > 0 && sellstop > 0) {
      isOrderPlaced = true;
   }
}
